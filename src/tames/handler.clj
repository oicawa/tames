(ns tames.handler
  (:gen-class)
  (:use [ring.adapter.jetty])
  (:require [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.util.response :as response]
            [tames.fsutils :as fs]
            [tames.config :as config]
            [tames.system :as system]
            [tames.unique :as unique]
            [tames.html :as html]
            [tames.authentication :as authentication])
  (:import (java.net URLDecoder URLEncoder)))

(defn other-resources
  [req]
  (let [relative-path     (get-in req [:route-params :*] nil)
        not-resource?     (. relative-path startsWith "data/")
        config-resource?  (and (not (. relative-path endsWith ".js"))
                               (not (. relative-path endsWith ".css"))
                               (. relative-path startsWith (format "%s/" (config/site-name))))
        if-modified-since (get-in req [:headers "if-modified-since"] nil)
        ext               (fs/ext relative-path)
        content-type      (html/content-types (. ext toLowerCase))
        file              (if config-resource?
                              (config/resource-file relative-path)
                              (fs/get-target-file (config/package-paths) relative-path))
        last-modified     (html/time-to-RFC1123 (. file lastModified))
        not-modified?     (= if-modified-since last-modified)]
    (log/debugf "Route [/*.%s] -> [%s]" ext (. file getAbsolutePath))
    (cond not-resource?         (-> (response/response nil)
                                    (response/status 403))
          (not (. file exists)) (route/not-found "Not Found")
          not-modified?         (-> (response/response nil)
                                    (response/status 304))
          :else                 (-> (response/file-response (. file getAbsolutePath))
                                    (response/header "Content-Type" content-type)
                                    (response/header "Last-Modified" last-modified)))))

(defroutes app-routes
  ;; Authentication
  (POST "/login" req
    (log/debug "[POST] /login")
    (authentication/login req))
  (GET "/logout" req
    (log/debug "[GET] /logout")
    (authentication/logout req))

  ;; Portal Top
  (GET ["/:site-name" :site-name #"[a-zA-Z][-_a-zA-Z0-9]*"] req
    (let [site-name (get-in req [:route-params :site-name] nil)]
      (log/debugf "[GET] /%s" site-name)
      (if (= site-name (config/site-name))
          (html/top-page)
          (route/not-found "Not Found"))))
  
  ;; REST API for CRUD
  (GET "/api/rest/:class-id" req
    (let [class-id          (get-in req [:route-params :class-id] nil)
          if-modified-since (get-in req [:headers "if-modified-since"] nil)
          exists?           (system/exists? unique/CLASS_ID class-id)
          cache?            (system/use-cache? class-id)
          last-modified     (html/time-to-RFC1123 (system/get-last-modified class-id nil))
          not-modified?     (= if-modified-since last-modified)]
      (log/debugf "[GET] /api/rest/%s" class-id)
      (cond (not exists?)
              (-> (response/response nil) (response/status 410))
            (not cache?)
              (system/get-data class-id nil)
            not-modified?
              (-> (response/response nil) (response/status 304))
            :else
              (-> (system/get-data class-id nil)
                  (response/header "Last-Modified" last-modified)))))
  (GET "/api/rest/:class-id/:object-id" req
    (let [class-id          (get-in req [:route-params :class-id] nil)
          object-id         (get-in req [:route-params :object-id] nil)
          if-modified-since (get-in req [:headers "if-modified-since"] nil)
          exists?           (system/exists? class-id object-id)
          cache?            (system/use-cache? class-id)
          last-modified     (html/time-to-RFC1123 (system/get-last-modified class-id object-id))
          not-modified?     (= if-modified-since last-modified)]
      (log/debugf "[GET] /api/rest/%s/%s" class-id object-id)
      (cond (not exists?)
              (-> (response/response nil) (response/status 410))
            (not cache?)
              (system/get-data class-id object-id)
            not-modified?
              (-> (response/response nil) (response/status 304))
            :else
              (-> (system/get-data class-id object-id)
                  (response/header "Last-Modified" last-modified)))))
  (POST "/api/rest/:class-id" [class-id & params]	;;; https://github.com/weavejester/compojure/wiki/Destructuring-Syntax
    (log/infof "[POST] /api/rest/%s" class-id)
    (if (not (system/exists? unique/CLASS_ID class-id))
        (-> (response/response nil)
            (response/status 410))
        (let [json-str    (URLDecoder/decode (params :value) "UTF-8")
              data        (json/read-str json-str)
              added-files (dissoc params :value)]
          (system/post-data class-id data added-files))))
  (PUT "/api/rest/:class-id/:object-id" [class-id object-id & params]
    (log/infof "[PUT] /api/rest/%s/%s" class-id object-id)
    (if (not (system/exists? class-id object-id))
        (-> (response/response nil)
            (response/status 410))
        (let [json-str     (URLDecoder/decode (params :value) "UTF-8")
              data         (json/read-str json-str)
              added-files  (dissoc params :value)]
          (system/put-data class-id object-id data added-files))))
  (DELETE "/api/rest/:class-id/:object-id" [class-id object-id]
    (log/infof "[DELETE] /api/rest/%s/%s" class-id object-id)
    (if (not (system/exists? class-id object-id))
        (-> (response/response nil)
            (response/status 410))
        (system/delete-data class-id object-id)))
  ;; Session
  (GET "/api/session" req
    (let [session  (req :session)
          json     (json/write-str session)]
          ;encoded (URLEncoder/encode json "UTF-8")]
      (log/debugf "[GET] /session")
      (-> (response/response json)
          (response/header "Contents-Type" "text/json; charset=utf-8"))))
  ;; Download
  (GET "/api/download/:class-id/:object-id/*" [class-id object-id & params]
    (log/debugf "[GET] /api/download/%s/%s/%s" class-id object-id (params :*))
    (let [file        (system/get-attachment-file class-id object-id (params :*))
          file-name   (. file getName)
          encoded-file-name (. (URLEncoder/encode file-name "UTF-8") replace "+" "%20")
          disposition (format "attachment;filename=\"%s\";filename*=UTF-8''%s" file-name encoded-file-name)
          ]
      (-> (response/file-response (. file getAbsolutePath))
          (response/header "Content-Type" "application/octet-stream")
          (response/header "Content-Disposition" disposition))))
  (GET "/api/image/:class-id/:object-id/*" [class-id object-id & params]
    (log/debugf "[GET] /api/image/%s/%s/%s" class-id object-id (params :*))
    (let [path (. (system/get-attachment-file class-id object-id (params :*)) toString)
          ext  (fs/ext path)
          mime (html/content-types ext)]
      (-> (response/file-response path)
          (response/header "Content-Type" mime))))
  (POST "/api/generate/:generator-name" [generator-name & params]
    (log/infof "[POST] /api/generate/%s" generator-name)
    (let [namespace-name    (format "tames.generators.%s" generator-name)
          generate-symbol   (symbol namespace-name "generate")
          get-content-type-symbol (symbol namespace-name "get-content-type")
          json-str          (URLDecoder/decode (params :value) "UTF-8")
          data              (json/read-str json-str)
          title             (data "title")
          tmp-file          (fs/create-tmp-file title generator-name)
          file-name         (format "%s.%s" title generator-name)
          encoded-file-name (. (URLEncoder/encode file-name "UTF-8") replace "+" "%20")
          disposition       (format "attachment;filename=\"%s\";filename*=UTF-8''%s" file-name encoded-file-name)
          ]
      (require (symbol namespace-name))
      (apply (find-var generate-symbol) [tmp-file data])
      (-> (response/file-response (. tmp-file getAbsolutePath))
          (response/header "Content-Type" (apply (find-var get-content-type-symbol) []))
          (response/header "Content-Disposition" disposition))))
  (POST "/api/operation/:operator-name/:operation-name" [operator-name operation-name & params]
    (log/infof "[POST] /api/operation/%s/%s" operator-name operation-name)
    (let [namespace-name   (format "tames.operations.%s" operator-name)
          operation-symbol (symbol namespace-name operation-name)
          json-str         (URLDecoder/decode (params :value) "UTF-8")
          data             (json/read-str json-str)]
      (require (symbol namespace-name))
      (apply (find-var operation-symbol) [data])))
  
  ;; Others (Resources & Public API)
  (GET "/*" req
    (log/debugf "[GET] /%s" (get-in req [:route-params :*] nil))
    (other-resources req))
  ; TODO Use package name.
  ;(POST "/:package-name/operation/:operator-name/:operation-name" [package-name operator-name operation-name & params]
  (POST "/public_api/operation/:operator-name/:operation-name" [operator-name operation-name & params]
    (log/infof "[POST] /public_api/operation/%s/%s" operator-name operation-name)
    (let [namespace-name   (format "tames.operations.%s" operator-name)
          operation-symbol (symbol namespace-name operation-name)
          json-str         (URLDecoder/decode (params :value) "UTF-8")
          data             (json/read-str json-str)]
      (require (symbol namespace-name))
      (apply (find-var operation-symbol) [data])))
  
  ;; Not Found
  (route/not-found "Not Found"))

