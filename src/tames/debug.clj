(ns tames.debug
  (:require [clojure.tools.logging :as log]))

(defn debug-pprint
  [x]
  `(let [res#    ~x
         stacks# (. (Throwable.) getStackTrace)
         stack#  ((vec stacks#) 0)
         file#   (. stack# getFileName)
         line#   (. stack# getLineNumber)]
    (log/infof "?=[%s, Line:%d]" file# (+ line# 1))
    (log/spy :info res#)
    res#))
