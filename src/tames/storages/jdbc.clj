(ns tames.storages.jdbc
  (:require [clojure.string :as string])
  (:import (java.io File)
           (java.net URL URLClassLoader)
           (java.sql DriverManager Driver)
           (java.util Properties)))

(def storages {})

(defn get-connection
  [storage-config]
  (let [storage-id   (storage-config "id")
        storage      (storages storage-id)
        driver       (storage :driver)
        url          (storage :url)
        properties   (storage :properties)]
    (if (nil? driver)
        nil
        (doto (. driver connect url properties)
          (.setAutoCommit false)))))

;DROP TABLE IF EXISTS TEST;
;CREATE TABLE TEST(ID INT PRIMARY KEY, NAME VARCHAR(255));
;INSERT INTO TEST VALUES(1, 'Hello');
;INSERT INTO TEST VALUES(2, 'World');
;SELECT * FROM TEST ORDER BY ID;
;UPDATE TEST SET NAME='Hi' WHERE ID=1;
;DELETE FROM TEST WHERE ID=2;

(defn update
  [storage-config queries]
  (let [connection (get-connection storage-config)]
    (try
      (doseq [query queries]
        (let [statement          (. connection prepareStatement (query :sql))
              indexed-parameters (map-indexed vector (query :parameters))]
          (try
            (doseq [[i parameter] indexed-parameters]
              (. statement setObject (+ i 1) parameter))
            (. statement executeUpdate)
            (finally
              (. statement close)))))
      (. connection commit)
      (catch Exception _
        (. connection rollback))
      (finally
        (. connection close)))))

(defn query-create-table-field-vector
  [fields]
  (loop [field       (first fields)
         rest-fields (rest fields)
         field-vector  []]
    (if (nil? field)
        field-vector
        (recur (first rest-fields)
               (rest rest-fields)
               (conj field-vector (format "%s %s" (field :name) (field :type)))))))

(defn create-table
  "Create a new table"
  [storage-config table-name fields]
  (let [fields-query (string/join "," (query-create-table-field-vector fields))
        connection   (get-connection storage-config)
        statement    (. connection createStatement)]
    (. statement executeUpdate (format "create table %s (%s)" table-name fields-query))))

(defn delete-table
  "Drop the table"
  [storage-config table-name]
  (let [connection (get-connection storage-config)
        statement  (. connection createStatement)]
    (. statement executeUpdate (format "drop table %s" table-name))))

(defn create-storage
  [storage-config]
  (let [; configuration parameters
        storage-id   (storage-config "id")
        jar-path     (storage-config "jar_path")
        driver-class (storage-config "driver_class")
        url          (storage-config "url")
        user         (storage-config "user")
        password     (storage-config "password")
        db-name      (storage-config "db_name")
        ; resources
        jar-file     (File. jar-path)
        url-array    (into-array URL [(.. jar-file (toURI) (toURL))])
        loader       (URLClassLoader/newInstance url-array)
        loaded-class (. loader loadClass driver-class)
        driver       (cast Driver (. loaded-class newInstance))
        properties   (doto (Properties.)
                       (.setProperty "user" user)
                       (.setProperty "password" password))]
    (assoc storages storage-id {:driver driver :url url :properties properties})
    ; Create database if not exists
    (update storage-config [{:sql "create database if not exists ?" :parameters [db-name]}])))

(defn init
  []
  true)  