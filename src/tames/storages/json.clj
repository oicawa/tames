(ns tames.storages.json
  (:gen-class)
  (:use ring.adapter.jetty)
  (:require [clojure.java.io :as io]
            [ring.util.response :as response]
            [clojure.data.json :as json]
            [clojure.string :as string]
            [clojure.tools.logging :as log]
            [tames.config :as config]
            [tames.fsutils :as fs]
            [tames.unique :as unique])
  (:import (java.io File InputStream)
           (java.nio.file Paths Path Files StandardCopyOption)
           (java.util UUID Calendar)))

;;; ------------------------------------------------------------
;;; JSON files
;;; ------------------------------------------------------------
(defn get-json-file
  [class-id object-id]
  (fs/get-target-file (config/package-paths) (format "data/%s/%s.json" class-id object-id)))

(defn get-json-dir
  [class-id]
  (fs/get-target-dir (config/package-paths) (format "data/%s" class-id)))

(defn assoc-files
  [dic files]
  (if (empty? files)
      dic
      (let [file (first files)]
        (recur (assoc dic (. file getName) file) (rest files)))))
  
(defn get-json-files
  [class-id]
  (loop [files      {}
         class-dirs (fs/get-target-dirs (config/package-paths) (format "data/%s" class-id))]
    (if (empty? class-dirs)
        (vals files)
        (let [class-dir (first class-dirs)
              rest-dirs (rest class-dirs)
              tmp-files (filter #(and (. %1 isFile)
                                      (nil? (files (. %1 getName))))
                                (. class-dir listFiles))]
          (recur (assoc-files files tmp-files) rest-dirs)))))


;;; ------------------------------------------------------------
;;; File & Directory
;;; ------------------------------------------------------------
(defn exists?
  [class-id object-id]
  (if (config/id? class-id)
      true
      (let [dir  (fs/get-target-dir (config/package-paths) (format "data/%s" class-id))
            file (get-json-file class-id object-id)]
        (cond (not (. dir isDirectory)) false
              (nil? object-id)          true
              :else                     (. file isFile)))))

(defn get-file-contents
  [path]
  { "file_path" path "file_contents" (slurp path) })

(defn get-last-modified
  [class-id object-id]
  (cond (config/id? class-id)
          (config/last-modified)
        (nil? object-id)
          (let [dirs      (fs/get-target-dirs (config/package-paths) (format "data/%s" class-id))
                files     (get-json-files class-id)
                all-items (sort #(- (. %2 lastModified) (. %1 lastModified))
                                (concat dirs files))]
            (. (first all-items) lastModified))
        :else
          (. (get-json-file class-id object-id) lastModified)))

;;; ------------------------------------------------------------
;;; Object
;;; ------------------------------------------------------------
(defn get-object
  [class-id object-id]
  (let [file (if (config/id? class-id)
                 (File. (config/path))
                 (get-json-file class-id object-id))]
    (if (not (. file exists))
        nil
        (with-open [rdr (io/reader (. file getAbsolutePath))]
          (json/read rdr)))))

(defn get-objects
  [class-id]
  (let [files   (get-json-files class-id)
        objects (map #(with-open [rdr (io/reader (. %1 getAbsolutePath))]
                        (json/read rdr))
                     files)
        ids     (map #(%1 "id") objects)]
    (zipmap ids objects)))

(defn create-object
  [class-id object-id s-exp-data]
  (let [file (get-json-file class-id object-id)
        dir  (. file getParentFile)]
    (fs/ensure-directory (. dir toString))
    (with-open [w (io/writer (. file toString))]
      (json/write s-exp-data w))))

(defn update-object
  [class-id object-id s-exp-data]
  (let [file (if (config/id? class-id)
                 (File. (config/path))
                 (get-json-file class-id object-id))]
    ;; !! CAUTION !!
    ;; Implement 's-exp-data' check logic!!
    (with-open [w (io/writer file)]
      (json/write s-exp-data w)
      (if (config/id? class-id) (config/update)))))

(defn delete-object
  [class-id object-id]
  (let [file (get-json-file class-id object-id)]
    (fs/delete file)
    (if (= unique/CLASS_ID class-id)
        (let [data-dir (get-json-dir object-id)]
          (fs/delete data-dir)))))

(defn get-object-as-json
  [class-id object-id]
  (json/write-str (get-object class-id object-id)))

(defn get-objects-as-json
  [class-id]
  (let [objects (get-objects class-id)]
    (json/write-str objects)))

(defn init
  []
  true)

