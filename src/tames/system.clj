(ns tames.system
  (:gen-class)
  (:use ring.adapter.jetty)
  (:require [clojure.java.io :as io]
            [ring.util.response :as response]
            [clojure.data.json :as json]
            [clojure.string :as string]
            [clojure.tools.logging :as log]
            [tames.config :as config]
            [tames.fsutils :as fs]
            [tames.storages.json :as storage]
            [tames.unique :as unique])
  (:import (java.io File InputStream)
           (java.nio.file Paths Path Files StandardCopyOption)
           (java.util Calendar)))

;;; ------------------------------------------------------------
;;; Attachment
;;; ------------------------------------------------------------
(defn get-attachment-base-dirs
  [class-id object-id]
  (if (config/id? class-id)
      [(config/get-attachment-dir)]
      (fs/get-target-dirs (config/package-paths) (format "data/%s/.%s" class-id object-id))))

(defn get-attachment-base-dir
  [class-id object-id]
  (first (get-attachment-base-dirs class-id object-id)))

(defn get-attachment-dirs
  [class-id object-id field_name]
  (if (config/id? class-id)
      [(config/get-attachment-dir field_name)]
      (fs/get-target-dirs (config/package-paths) (format "data/%s/.%s/%s" class-id object-id field_name))))

(defn get-attachment-dir
  [class-id object-id field_name]
  (first (get-attachment-dirs class-id object-id field_name)))

(defn get-attachment-file
  [class-id object-id field_and_file_name]
  (if (config/id? class-id)
      (config/get-attachment-file field_and_file_name)
      (fs/get-target-file (config/package-paths) (format "data/%s/.%s/%s" class-id object-id field_and_file_name))))

(defn get-files-fields
  [class-id]
  (let [class_ (storage/get-object unique/CLASS_ID class-id)]
    (filter #(let [id ((%1 "datatype") "id")]
               (or (= id unique/FILES_ID)
                   (= id unique/IMAGES_ID)))
            (class_ "object_fields"))))

(defn remove-attached-files
  [class-id object-id value files_fields]
  (doseq [field files_fields]
    (let [dst-dir    (get-attachment-dir class-id object-id (field "name"))
          file-names (keys ((value (field "name")) "remove"))]
      (doseq [file-name file-names]
        (let [file (File. dst-dir file-name)]
          (if (. file exists)
              (. file delete)))))))

(defn save-attached-files
  [class-id object-id value files_fields added-files]
  (doseq [field files_fields]
    (let [dst-dir   (get-attachment-dir class-id object-id (field "name"))
          file-keys (keys ((value (field "name")) "added"))]
      (fs/ensure-directory dst-dir)
      (doseq [file-key file-keys]
        (let [file      (added-files (keyword file-key))
              tmp-file  (file :tempfile)
              file-name (. (File. (file :filename)) getName)
              dst-file  (.. (fs/to-path dst-dir) (resolve file-name) (toFile))]
          (io/copy tmp-file dst-file))))))

(defn update-files-values
  [class-id object-id files_fields raw-value]
  (let [base-dir-path (get-attachment-base-dir class-id object-id)
        field_names   (map #(%1 "name") files_fields)]
    (loop [names field_names
           value raw-value]
      (if (empty? names)
          value
          (let [name     (first names)
                dir      (fs/to-file (fs/make-path base-dir-path name))
                current  (map (fn [file] { "name" (. file getName) "size" (. file length) })
                              (vec (if (and (not (nil? dir)) (. dir exists) (. dir isDirectory))
                                       (. dir listFiles)
                                       [])))
                value1  (dissoc value name)
                value2  (assoc value name {"class_id" class-id "object_id" object-id "current" current})]
            (recur (rest names) value2))))))


;;; ------------------------------------------------------------
;;; Access
;;; ------------------------------------------------------------
(defn get-account
  [login_id]
  (let [accounts (filter #(= (%1 "login_id") login_id) (vals (storage/get-objects unique/ACCOUNT_ID)))
        account  (if (= (count accounts) 0) nil (first accounts))]
    account))

(defn is-user-in-group
  [account-id group-id]
  (let [group    (storage/get-object unique/GROUP_ID group-id)
        accounts (group "acounts")]
    (loop [rest-accounts accounts]
      (cond (empty? rest-accounts) false
            (= account-id (first rest-accounts)) true
            :else (recur (rest rest-accounts))))))

(defn exists?
  [class-id object-id]
  (storage/exists? class-id object-id))

(defn get-last-modified
  [class-id object-id]
  (storage/get-last-modified class-id object-id))

;;; ------------------------------------------------------------
;;; Called from handler
;;; ------------------------------------------------------------
(defn get-data
  [class-id object-id]
  (-> (response/response (if (nil? object-id)
                             (storage/get-objects-as-json class-id)
                             (storage/get-object-as-json class-id object-id)))
      (response/header "Contents-Type" "text/json; charset=utf-8")))

(defn post-data
  [class-id data added-files]
  (let [object-id    (unique/new-id)
        files_fields (get-files-fields class-id)
        data-with-id (assoc data unique/OBJECT_ID_NAME object-id)]
    (save-attached-files class-id object-id data-with-id files_fields added-files)
    (let [pure-data (update-files-values class-id object-id files_fields data-with-id)]
      (storage/create-object class-id object-id pure-data)
      (println "Posted OK.")
      (-> (response/response (storage/get-object-as-json class-id object-id))
          (response/header "Contents-Type" "text/json; charset=utf-8")))))

(defn put-data
  [class-id object-id data added-files]
  (let [files_fields (get-files-fields class-id)]
    (remove-attached-files class-id object-id data files_fields)
    (save-attached-files class-id object-id data files_fields added-files)
    (let [pure-data (update-files-values class-id object-id files_fields data)]
      (storage/update-object class-id object-id pure-data)
      (-> (response/response (storage/get-object-as-json class-id object-id))
          (response/header "Contents-Type" "text/json; charset=utf-8")))))

(defn delete-data
  [class-id object-id]
  (storage/delete-object class-id object-id)
  (fs/delete (get-attachment-base-dir class-id object-id))
  (-> (response/response (storage/get-objects-as-json class-id))
      (response/header "Contents-Type" "text/json; charset=utf-8")))

(defn use-cache?
  [class-id]
  (let [class-object (storage/get-object unique/CLASS_ID class-id)]
    (get-in class-object ["options" "cache"] false)))
