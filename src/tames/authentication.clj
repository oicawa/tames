(ns tames.authentication
  (:gen-class)
  (:require [clojure.data.json :as json]
            [clojure.tools.logging :as log]
            [buddy.auth :refer [authenticated?]]
            [ring.middleware.anti-forgery :refer [*anti-forgery-token*]]
            [ring.util.response :as response]
            [hiccup.core :refer [html]]
            [tames.operations.resource :as resource]
            [tames.config :as config]
            [tames.system :as system]
            [tames.html :as html]))

(defn page
  []
  (let [title                 (@config/data "system_lable")
        favicon-path          (config/favicon-path)
        modified-times        (vec (resource/get-properties-list ["core/main.js" "lib/require.js"]))
        main-last-modified    ((modified-times 0) "last-modified")
        require-last-modified ((modified-times 1) "last-modified")]
    (html
      [:head
        [:meta {:charset "utf-8"}]
        [:meta {:http-equiv "content-style-type" :content "text/css"}]
        [:meta {:http-equiv "content-script-type" :content "text/javascript"}]
        [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
        [:title title ]
        [:link {:rel "shortcut icon" :href favicon-path} ]
        (html/link-tag-list [
          "lib/jquery-ui-1.12.1.css"
          "lib/w2ui/w2ui-1.5.rc1.css"
          "lib/font-awesome-4.6.1/css/font-awesome.css"
          "core/reset-w2ui.css"
          "core/main.css"])
        [:script {:data-main (format "core/main.js?last-modified=%s" main-last-modified)
                  :src       (format "lib/require.js?last-modified=%s" require-last-modified)}]]
      [:body])))

(defn login
  [req]
  (let [login_id        (get-in req [:form-params "login_id"])
        password        (get-in req [:form-params "password"])
        redirect-url    (get-in req [:headers "referer"])
        login-try-count (get-in req [:session :login-try-count] 0)
        ;; Draft Implement...
        account         (system/get-account login_id)
        is-ok           (cond (nil? account) false
                              (= (account "password") password) true
                              :else false)]
    (log/infof "login_id=%s" login_id)
    (-> (response/redirect redirect-url)
        (assoc-in [:session :identity] (if is-ok login_id nil))
        (assoc-in [:session :uuid] (if is-ok (account "id") nil))
        (assoc-in [:session :login-try-count] (if is-ok login-try-count (+ login-try-count 1))))))

(defn logout
  [req]
  (-> (response/redirect (html/site-url))
      (assoc :session {})))

(defn response-authorized
  [authorized? login-try-count]
  (-> (response/response (if authorized?
                             nil
                             (json/write-str { "anti_forgery_token" (str *anti-forgery-token*)
                                               "login_try_count" login-try-count
                                               "redirect_url"    (html/site-url)
                                               "system_label"    (@config/data "system_label")
                                               "logo"            (config/logo-path)
                                               "favicon"         (config/favicon-path)
                                               "login"           (@config/data "login")})))
      (response/status (if authorized? 200 401))
      (response/header "Contents-Type" "text/json; charset=utf-8")))

(defn unauthorized
  [req meta]
  (let [result          (authenticated? req)
        uri             (req :uri)
        referer         ((req :headers) "referer")
        login-try-count (get-in req [:session :login-try-count] 0)]
    (log/debugf "*** Unauthenticated: [%s], URI: [%s], referer: [%s], login-try-count=[%d]" result uri referer login-try-count)
    (cond result
            (response/redirect (html/site-url))
          (= uri "/api/session/identity")
            (response-authorized false login-try-count)
          :else
            (response-authorized false login-try-count))))

