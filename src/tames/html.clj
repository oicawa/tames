(ns tames.html
  (:gen-class)
  (:require [hiccup.core :refer [html]]
            [tames.operations.resource :as resource]
            [tames.config :as config])
  (:import (java.text SimpleDateFormat)
           (java.util Locale TimeZone)))

(def content-types {""     ""
                    "css"  "text/css"
                    "js"   "text/javascript; charset=utf-8"
                    "html" "text/html; charset=utf-8"
                    "json" "text/json; charset=utf-8"
                    "svg"  "image/svg+xml"
                    "ico"  "image/x-icon"
                    "png"  "image/png"
                    "jpg"  "image/jpg"
                    "jpeg" "image/jpeg"
                    "pdf"  "application/pdf"
                    })

(defn link-tag
  [path last-modified-ISO8601]
  (let [href (format "%s?last-modified=%s" path last-modified-ISO8601)]
    [:link {:rel "stylesheet" :type "text/css" :href href } ]))
  
(defn link-tag-list
  [paths]
  (map #(link-tag (%1 "path") (%1 "last-modified"))
       (resource/get-properties-list paths)))

(defn top-page
  []
  (let [title                 (@config/data "system_lable")
        favicon-path          (config/favicon-path)
        modified-times        (vec (resource/get-properties-list ["core/main.js" "lib/require.js"]))
        main-last-modified    ((modified-times 0) "last-modified")
        require-last-modified ((modified-times 1) "last-modified")]
    (html
      [:head
        [:meta {:charset "utf-8"}]
        [:meta {:http-equiv "content-style-type" :content "text/css"}]
        [:meta {:http-equiv "content-script-type" :content "text/javascript"}]
        [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
        [:title title ]
        [:link {:rel "shortcut icon" :href favicon-path} ]
        (link-tag-list [
          "lib/jquery-ui-1.12.1.css"
          "lib/w2ui/w2ui-1.5.rc1.css"
          "lib/font-awesome-4.6.1/css/font-awesome.css"
          "core/reset-w2ui.css"
          "core/main.css"])
        [:script {:data-main (format "core/main.js?last-modified=%s" main-last-modified)
                  :src       (format "lib/require.js?last-modified=%s" require-last-modified)}]]
      [:body])))

(defn site-url
  []
  (format "/%s" (config/site-name)))

(defn time-to-RFC1123
  [time]
  (let [f   "EEE, dd MMM yyyy HH:mm:ss z"
        sdf (doto (SimpleDateFormat. f Locale/ENGLISH)
              (.setTimeZone (TimeZone/getTimeZone "GMT")))]
    (. sdf format time)))


