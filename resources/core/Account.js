define(function (require) {
  //require('jquery');
  //var Class = require('core/Class');
  //var Locale = require('core/Locale');
  var Connector = require("core/Connector");
  var Storage = require('core/Storage');
  
  function Account() {
  }
  
  Account.ID = "9643597d-5513-4377-961c-643293fa3319";
  
  Account.current = function () {
    var dfd = new $.Deferred;
    Connector.session()
    .then(function (session) {
      dfd.resolve(session.uuid);
    });
    return dfd.promise();
  };

  return Account;
});
