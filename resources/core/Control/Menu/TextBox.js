define(function (require) { 
  require("jquery");
  var Uuid = require("core/Uuid");
  
  var TextBox = {
    renderer : function(item) {
      var input_id = Uuid.version4();
      item.html = "<input id='" + input_id + "' type='text' class='filter' style='margin-left:5px;margin-right:5px;border-radius:3px;border:1px solid #cacaca;' />";
      this.owner.add(item)
      $(document).on("keydown", "#" + input_id, function (event) {
        if (event.keyCode !== 13) {
          return;
        }
        var filter_string = event.target.value;
        event.item = item;
        item.action(event);
        var input = $("#" + input_id);
        input.val(filter_string);
        input.focus();
      });
      return "";
    }
  };
  return TextBox;
});
