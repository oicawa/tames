define(function (require) { 
  require("jquery");
  var Utils = require("core/Utils");
  var Uuid = require("core/Uuid");
  var Menu = require("core/Control/Menu");
  var Toolbar = require("core/Control/Toolbar");
  var Detail = require("core/Control/Detail");
  var Dialog = require("core/Dialog");
  var Locale = require("core/Locale");
  var Storage = require("core/Storage");
  var Class = require("core/Class");

  var TEMPLATE = '<div class="tree-field"><div id="{{TOOLBAR_ID}}"></div><div id="{{TREE_ID}}"></div></div>';

  function TreeControl() {
    this._class = null;
    this._icon_field_name = null;
    this._objects = null;
    this._renderer = null;
    this._toolbar = null;
    this._sidebar = null;
    this._dialog_data = null;
    this._backup = null;
  }

  TreeControl.eq = function (source, condition) {
    var src_value = source[condition.field];
    if (!src_value) {
      return false;
    }
    return (src_value == condition.value) ? true : false;
  };

  TreeControl.neq = function (source, condition) {
    var eq = TreeControl.eq(source, condition);
    return eq ? false : true;
  };

  TreeControl.append = function (event) {
    console.log("TreeControl.append()");
    var tree = event.item.context;
    var id = tree.selection();
    tree.show_item(null, function(data) {
      var text = tree.render(data);
      tree.append(id, [ { id : Uuid.version4(), text : text, data : data } ]);
      tree.refresh();
    });
  };

  TreeControl.insert = function (event) {
    console.log("TreeControl.insert()");
  };
  
  TreeControl.delete = function (event) {
    console.log("TreeControl.delete()");
    var tree = event.item.context;
    var id = tree.selection();
    var item = event.item;
    var entry_props = !item.function_entry ? null : item.function_entry.properties;
    
    var message = !entry_props ? "Do you want to delete?" : Locale.translate(entry_props.confirm_message);
    Dialog.confirm(message, item.text)
    .yes(function() {
      tree.delete(id);
      tree.refresh();
    });
  };

  TreeControl.edit = function (event) {
    console.log("TreeControl.edit()");
    var tree = event.item.context;
    var id = tree.selection();
    tree.show_item(id, function(data) {
      var text = tree.render(data);
      tree.set(id, { id : Uuid.version4(), text : text, data : data });
      tree.refresh();
    });
  };

  function create_sidebar(uuid, self) {
    var tree = $("#" + uuid);
    tree.on("remove", function(event) {
      delete w2ui[uuid];
    });
    
    var sidebar = tree.w2sidebar({
      name : uuid,
      menu : [],
      nodes : [],
      style: "height:300px;width:600px;",
      onContextMenu: function(event) {
        var node = sidebar.get(event.target);
        sidebar.menu = self._menus.filter(function (menu) {
          return TreeControl[menu.filter.op](node, menu.filter);
        });
      },
      onMenuClick: function(event) {
        var menu = event.menuItem;
        if (!menu.run) {
          console.log("onMenuClick:[" + menu.text + "]");
          return;
        }
        menu.run(sidebar, event.target);
      },
      onDestroy: function(event) {
        //delete w2ui[uuid];
      },
      onRender : function (event) {
        console.log("Called onRender(...)");
      },
      onClick : function (event) {
        if (event.target !== sidebar.selected) {
          return;
        }
        event.onComplete = function(after_event) {
          sidebar.unselect();
        }
      }
    });
    self._sidebar = w2ui[uuid];
  };

  function create_toolbar(uuid, field, self) {
    var dfd = new $.Deferred;
    var properties = field.datatype.properties;
    if (is_null_or_undefined(properties) || is_null_or_undefined(properties.menus)) {
      dfd.resolve();
      return dfd.promise();
    }
    var src_menu_ids = properties.menus;
    self._toolbar = new Toolbar();
    self._toolbar.init("#" + uuid)
    .then(function () {
      return Menu.convert(src_menu_ids, self).done(function(dst_items) { self._toolbar.items(dst_items); });
    })
    .then(function () {
      self._toolbar.visible(true);
    })
    .then(function () {
      dfd.resolve();
    });
    return dfd.promise();
  };

  function load(self, field) {
    var dfd = new $.Deferred;
    var properties = field.datatype.properties;
    if (is_null_or_undefined(properties) || is_empty(properties.class_id)) {
      dfd.resolve();
      return dfd.promise();
    }
    self._embedded = properties.embedded;
    $.when(
      Storage.read(Class.CLASS_ID, properties.class_id).then(function (data) { self._class = data; }),
      Storage.read(properties.class_id).then(function (data) { self._objects = data; })
    )
    .then(function () {
      return (new Class(self._class)).renderer("hoge");
    })
    .then(function (renderer) {
      self._renderer = renderer;
    })
    .then(function () {
      dfd.resolve();
    })
    return dfd.promise();
  };

  TreeControl.prototype.destroy = function() {
    this._toolbar.destroy();
    this._sidebar.destroy();
  };

  TreeControl.prototype.init = function(selector, field) {
    var toolbar_id = Uuid.version4();
    var sidebar_id = Uuid.version4();
    var html = TEMPLATE.replace(/{{TOOLBAR_ID}}/, toolbar_id).replace(/{{TREE_ID}}/, sidebar_id);
    var root = $(selector);
    root.append(html);

    var self = this;
    return load(this, field)
    .then(function () {
      create_sidebar(sidebar_id, self);
      return;
    })
    .then(function () {
      return create_toolbar(toolbar_id, field, self);
    })
    //.then(function () {
    //  self._menus = [
    //    {id:1, text:"Append", img:null, icon:null, filter:{op:"eq",  field:"img", value:"icon-folder"}, run:Tree.append },
    //    {id:2, text:"Remove", img:null, icon:null, filter:{op:"eq",  field:"img", value:"icon-folder"}, run:Tree.remove },
    //    {id:3, text:"Insert", img:null, icon:null, filter:{op:"neq", field:"img", value:"icon-folder"}, run:Tree.insert },
    //    {id:4, text:"Remove", img:null, icon:null, filter:{op:"neq", field:"img", value:"icon-folder"}, run:Tree.remove },
    //  ];
    //})
    .then(function () {
      self.refresh();
    });
  };

  TreeControl.prototype.show_item = function (item_id, ok_proc) {
    var self = this;
    var object = null;
    var detail = new Detail();
    var dialog = new Dialog();
    dialog.init(function(id) {
      var dfd = new $.Deferred;
      detail.init('#' + id, self._class.object_fields)
      .then(function () {
        detail.edit(true);
        detail.visible(true);
        if (is_empty(item_id) === false) {
          var data =  self._sidebar.get(item_id).data;
          detail.data(data);
        }
        detail.refresh();
        dfd.resolve();
      });
      return dfd.promise();
    }).then(function () {
      dialog.title("!!! Implement !!!");
      dialog.buttons([
        {
          text : "OK",
          click: function (event) {
            ok_proc(detail.data());
            dialog.close();
            return false;
          }
        },
        {
          text : "Cancel",
          click: function (event) {
            dialog.close();
            return false;
          }
        }
      ]);
      dialog.open();
    });
  };

  //TreeControl.prototype.convert = function (items) {
  //  var nodes = items.map(function (item) {
  //    var class_id = item["class_id"];
  //    var renderer = this._renderers[class_id];
  //    return { id: item["id"], text: renderer(item) };
  //  });
  //  return nodes;
  //}
  
  TreeControl.prototype.selection = function() {
    return this._sidebar.selected;
  };
  
  TreeControl.prototype.append = function(parent_id, items) {
    console.assert(is_null_or_undefined(this._sidebar) === false, "'_sidebar' is null or empty...");
    console.assert(is_array(items), "'items' not array...");
    console.assert(0 < items.length, "'items' length equals ZERO...");
    var nodes = items;
    if (is_null_or_undefined(parent_id)) {
      this._sidebar.add(nodes);
    } else {
      this._sidebar.add(parent_id, nodes);
      this._sidebar.expand(parent_id);
    }

    this._sidebar.select(nodes[0].id);
  };

  TreeControl.prototype.insert = function(parent_id, before_id, items) {
    if (this._sidebar === null) {
      return;
    }
    var nodes = items;
    this._sidebar.insert(parent_id, before_id, nodes);
  };

  TreeControl.prototype.delete = function(id) {
    if (this._sidebar === null) {
      return;
    }
    this._sidebar.remove(id);
  };

  TreeControl.prototype.edit = function(on) {
    console.log("Tree.prototype.edit(on) called with 'on'=" + on);
  };

  function extract(node, self) {
    var text = (typeof self._renderer === "function" ? self._renderer(node.data) : "")
    var dst = { "id" : node.id, "text" : text, "data" : node.data };
    if (is_array(node.nodes)) {
      dst.nodes = node.nodes.map(function(node) { return extract(node, self); });
    }
    return dst;
  }

  TreeControl.prototype.refresh = function(target_id) {
    if (this._toolbar !== null) {
      this._toolbar.refresh();
    }
    
    if (this._sidebar === null) {
      return;
    }

    if (is_empty(target_id)) {
      this._sidebar.refresh();
    } else {
      this._sidebar.refresh(target_id);
    }
  };

  TreeControl.prototype.backup = function() {
    return Utils.clone(this._backup);
  };

  TreeControl.prototype.commit = function() {
    var self = this;
    this._backup = this._sidebar.nodes.map(function(node) { return extract(node, self); });
  };

  TreeControl.prototype.clear = function (){
    var top_ids = this._sidebar.nodes.map(function (node) { return node.id; });
    this._sidebar.remove.apply(this._sidebar, top_ids);
  }

  TreeControl.prototype.restore = function() {
    this.clear();
    this._sidebar.add(Utils.clone(this._backup));
  };

  TreeControl.prototype.set = function(id, item) {
    this._sidebar.set(id, item);
  };

  TreeControl.prototype.data = function(value) {
    var self = this;
    if (arguments.length === 0) {
      if (is_null_or_undefined(this._sidebar)) {
        return [];
      }
      return this._sidebar.nodes.map(function(node) { return extract(node, self); });
    } else {
      this._backup = is_array(value) ? Utils.clone(value) : [];
      this._sidebar.add(Utils.clone(this._backup));
    }
  };
  
  TreeControl.prototype.update = function(keys) {
  };
  
  TreeControl.prototype.render = function(object) {
    return this._renderer(object);
  };

  TreeControl.dummy_data = function () {
    return [
      { id: 'z', text: 'Item 1', img: 'icon-folder', nodes: [
        { id: 'm', text: 'Item 1-1', icon: 'fa-star-empty' },
        { id: 'd', text: 'Item 1-2', icon: 'fa fa-question-circle-o' }
      ]},
      { id: 'a', text: 'Item 2', img: 'icon-folder', nodes: [
        { id: 's', text: 'Item 2-1', icon: 'fa-star-empty' },
        { id: 'g', text: 'Item 2-2', icon: 'w2ui-icon-check' }
      ]}
    ];
  };

  TreeControl.sample_w2ui_items = function(tree) {
    tree.append(null, [
      { id: 'z', text: 'Item 1' },
      { id: 'a', text: 'Item 2' }
    ]);
    tree.insert('a', null, [
      { id: 'm', text: 'Item 1-1' },
      { id: 'd', text: 'Item 1-2' }
    ]);
    tree.insert('z', null, [
      { id: 's', text: 'Item 2-1' },
      { id: 'g', text: 'Item 2-2' }
    ]);
    tree.refresh();
  }

  return TreeControl;
});
