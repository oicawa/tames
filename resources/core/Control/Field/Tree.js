define(function (require) { 
  require("jquery");
  var Field = require("core/Control/Field/Field");
  var Inherits = require("core/Inherits");
  var TreeControl = require("core/Control/TreeControl");

  function Tree() {
    Field.call(this, "core/Control/Field", "Tree");
    this._tree = null;
  }
  Inherits(Tree, Field);

  Tree.prototype.destroy = function() {
    this._tree.destroy();
  };

  Tree.prototype.init = function(selector, field) {
    var dfd = new $.Deferred;

    var self = this;
    Field.prototype.init.apply(this, arguments) // call super
    .then(function () {
      self._tree = new TreeControl();
      return self._tree.init(selector, field);
    })
    .then(function () {
      dfd.resolve();
    });
    return dfd.promise();
  };
  
  Tree.prototype.convert = function (items) {
    var nodes = items.map(function (item) {
      var class_id = item["class_id"];
      var renderer = this._renderers[class_id];
      return { id: item["id"], text: renderer(item) };
    });
    return nodes;
  }

  Tree.prototype.restore = function() {
  };

  Tree.prototype.refresh = function() {
    this._tree.refresh();
  };

  Tree.prototype.commit = function() {
  };

  Tree.prototype.edit = function(on) {
    console.log("Tree.prototype.edit(on) called with 'on'=" + on);
  };

  Tree.prototype.refresh = function(target_id) {
    //debugger;
    //var target_item = this._data[!target_id ? "" : target_id];
    //{ "data" : data, "order" : ["id-1", "id-2", "id-3"] },
    //var data = target_item["data"];
    //var order = target_item["order"];
    if (!target_id) {
      this._tree.refresh();
    } else {
      this._tree.refresh(target_id);
    }
  };

  Tree.prototype.data = function(value) {
    if (arguments.length == 0) {
      return this._tree.data();
    } else {
      this._tree.data(value);
    }
  };
  
  Tree.prototype.update = function(keys) {
  };

  return Tree;
});
