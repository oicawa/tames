define(function (require) { 
  require("jquery");
  var Utils = require("core/Utils");
  var Locale = require("core/Locale");
  var Class = require("core/Class");
  var Storage = require("core/Storage");
  var Inherits = require("core/Inherits");
  var Field = require("core/Control/Field/Field");
  var Detail = require("core/Control/Detail");
  var DivButton = require("core/Control/DivButton");
  var Finder = require("core/Control/Finder");
  var Grid = require("core/Control/Grid");
  var Dialog = require("core/Dialog");
  var Uuid = require("core/Uuid");
  
  var TEMPLATE = '' +
'<div>' +
'  <div name="finder" style="display:inline-block;"></div><span></span>' +
//'  <div name="field" class="w2ui-field"></div>' +
'  <div name="field" ></div>' +
'</div>';

  var OPTION_TEMPLATE = '<option></option>';

  function create_finder(self, selector, columns, field_map, items, description, min_width, renderer) {
    self._finder = new Finder();
    return self._finder.init(selector, columns, field_map, items, description, false, min_width, renderer);
  }

  function create_renderer(self) {
    var dfd = new $.Deferred;
    var id = self._finder.data();
    console.log(id);
    var field = { name : "data", datatype : { properties : self._properties } };
    var Control = Field.control(id);
    if (is_null_or_undefined(Control) || is_null_or_undefined(Control.renderer)) {
      dfd.resolve(null)
      return dfd.promise();
    }
    Control.renderer(field)
    .then(function (render) {
      dfd.resolve(render)
    });
    return dfd.promise();
  }

  function create_button(self, selector, root) {
    self._button = new DivButton();
    return self._button.init(selector, "<i class='fa fa-th-list'/>", function (evnet) {
      var primitive = null;
      var detail = new Detail();
      var dialog = new Dialog();
      dialog.init(function(id) {
        var dfd = new $.Deferred;
        
        primitive = self._fieldcontrols[self._finder.data()];
        detail.init('#' + id, primitive["properties"])
        .then(function () {
          detail.data(self._properties);
          detail.edit(true);
          detail.refresh();
          detail.visible(true);
          dfd.resolve();
        });
        return dfd.promise();
      }).then(function () {
        dialog.title(Locale.translate(primitive.label));
        dialog.buttons([
          {
            text : "OK",
            click: function (event) {
              self._properties = detail.data();
              dialog.close();
              self._value.properties = self._properties;
              create_field(self, root);
              create_renderer(self).then(function (render) { self._render = render; });
              return false;
            }
          },
          {
            text : "Cancel",
            click: function (event) {
              dialog.close();
              return false;
            }
          }
        ]);
        dialog.open();
      });
    });
  }
  function create_field(self, root) {
    var dfd = new $.Deferred;
    var field = root.find("div > div[name='field']");
    field.empty();
    var id = Uuid.version4();
    field.attr("id", id);
    
    var fieldcontrol = self._fieldcontrols[self._finder.data()];
    if (is_null_or_undefined(fieldcontrol)) {
      dfd.resolve();
      return dfd.promise();
    }
    var Control = Field.control(fieldcontrol.require_path);
    self._control = new Control();
    self._control.init("#" + id, { name : "", label : "", datatype : { properties : self._value.properties }}) // Need xxx.datatype....
    .then(function () {
      self._control.edit(self.edit());
      dfd.resolve();
    });
    return dfd.promise();
  }
  
  function AnyTypeValue() {
    Field.call(this, "core/Control/Field", "AnyTypeValue");
    this._selector = null;
    this._class = null;
    this._fieldcontrols = null;
    this._finder = null;
    this._button = null;
    this._field = null;
    this._value = null;
    this._items = {};
    this._properties = null;
    this._render = null;
    this._display = null;
  }
  Inherits(AnyTypeValue, Field);

  AnyTypeValue.prototype.init = function(selector, field) {
    var dfd = new $.Deferred;
    // Set member fields
    this._selector = selector;
    var root = $(selector);
    if (0 < root.children()) {
      dfd.resolve();
      return dfd.promise();
    }

    // Create form tags
    var self = this;
    var field_map = null;
    var columns = null;
    $.when(
      Storage.read(Class.CLASS_ID, Field.FIELDCONTROL_ID).done(function(data) { self._class = data; }),
      Storage.read(Field.FIELDCONTROL_ID).done(function(data) { self._fieldcontrols = data; })
    )
    .then(function () {
      return Class.field_map(self._class).then(function (field_map_) { field_map = field_map_; });
    })
    .then(function () {
      columns = Grid.columns(self._class, field_map);
    })
    .then(function() {
      return (new Class(self._class)).renderer();
    })
    .then(function(renderer) {
      root.empty();
      root.append(TEMPLATE);
      return create_finder(self, selector + " > div > div[name='finder']", columns, field_map, self._fieldcontrols, self._field_name, self._min_width, renderer);
    })
    .then(function() {
      self._finder.ok(function() {
        create_field(self, root);
        create_renderer(self).then(function (render) { self._render = render; });
      });
      self._finder.refresh();
    })
    .then(function() {
      return create_button(self, selector + " > div > span", root);
    })
    .then(function() {
        return create_renderer(self);
    })
    .then(function (render) {
      self._render = render;
    })
    .then(function() {
      self._button.visible(true);
      return create_field(self, root);
    })
    .then(function() {
      self._control && self._control.refresh();
      dfd.resolve();
    });
    return dfd.promise();
  };

  AnyTypeValue.prototype.edit = function(on) {
    if (arguments.length == 0) {
      return this._finder.edit();
    }
    this._finder.edit(on);
    if (this._control) {
      this._control.edit(on);
    }
  };
  
  AnyTypeValue.prototype.backup = function() {
    return this._value;
  };

  AnyTypeValue.prototype.commit = function() {
    if (this._control) {
      this._control.commit();
    }
    this._value = this.data();
  };

  AnyTypeValue.prototype.restore = function() {
    this._finder.data(this._value.id);
    this._properties = this._value.properties;
    if (this._control) {
      this._control.restore();
    }
    // set value.properties into this._detail in event handler.
  };

  AnyTypeValue.prototype.data = function(value) {
    if (arguments.length == 0) {
      // Getter
      var data = {
        "id" : this._finder.data(),
        "properties" : this._properties,
        "data" : is_null_or_undefined(this._control) ? null : this._control.data(),
      };
      data.display = this._render(data);
      return data;
    } else {
      // Setter
      this._value = is_null_or_undefined(value) ? { "id" : "", "properties" : null, "data" : null, "display" : "" } : value;
      this._properties = this._value.properties;
      this._display = this._value.display;
      this._finder.data(this._value.id);
      var root = $(this._selector);
      var self = this;
      create_field(this, root)
      .then(function () {
        if (self._control) {
          self._control.data(self._value.data);
        }
      })
      .then(function () {
        return create_renderer(self);
      })
      .then(function (render) {
        self._render = render;
      });
    }
  };
  
  AnyTypeValue.prototype.refresh = function() {
    this._finder.refresh();
    if (this._control) {
      this._control.refresh();
    }
    if (this._render) {
      this._display = this._render(this._control.data());
    }
  }
  
  AnyTypeValue.renderer = function(field) {
    var dfd = new $.Deferred;
    var renderer = function(record, index, column_index) {
      var value = record[field.name];
      return value.display;
    };
    dfd.resolve(renderer);
    return dfd.promise();
  };

  return AnyTypeValue;
}); 
