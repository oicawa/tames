define(function (require) { 
  require("jquery");
  var Storage = require("core/Storage");
  var Inherits = require("core/Inherits");
  var Control = require("core/Control/Control");
  var Uuid = require("core/Uuid");
  
  function Field(template_path, template_name) {
    Control.call(this, null, template_path, template_name);
    this._static = null;
  }
  Inherits(Field, Control);
  
  Field.FIELDCONTROL_ID = "1fd7625f-78b5-4079-95dd-951186cb79fe";

  var controls = null;
  var path2id = null;
  function get_all_fieldcontrols() {
    var dfd = new $.Deferred;
    if (controls !== null) {
      dfd.resolve(controls);
      return dfd.promise();
    }
    controls = {};
    path2id = {};
    Storage.read(Field.FIELDCONTROL_ID)
    .done(function (fieldcontrols) {
      var promises = Object.keys(fieldcontrols).map(function(object_id) {
        var fieldcontrol = fieldcontrols[object_id];
        var inner_dfd = new $.Deferred;
        require([fieldcontrol.require_path], function(Control) {
          controls[fieldcontrol.id] = Control;
          path2id[fieldcontrol.require_path] = fieldcontrol.id;
          inner_dfd.resolve();
        });
        return inner_dfd.promise();
      });
      $.when.apply(null, promises)
      .then(function() {
        dfd.resolve(controls);
      });
    });
    return dfd.promise();
  }

  Field.controls = function () {
    return get_all_fieldcontrols();
  }

  Field.control = function (id_or_path) {
    var uuid = Uuid.is_uuid(id_or_path) ? id_or_path : path2id[id_or_path];
    return controls[uuid];
  }

  Field.prototype.destroy = function() {
    
  };
  
  Field.prototype.init = function(selector, field) {
    var dfd = new $.Deferred;

    var self = this;
    self._statics = {};
    
    if (is_null_or_undefined(field.datatype)) {
      dfd.resolve();
      return dfd.promise();
    }
    
    if (is_empty(field.datatype.id)) {
      dfd.resolve();
      return dfd.promise();
    }
    
    Storage.read(Field.FIELDCONTROL_ID, field.datatype.id)
    .then(function(data) {
      self._statics = {};
      data.static_properties.forEach(function (property) {
        self._statics[property.key] = property.value.data;
      });
      dfd.resolve();
    });
    return dfd.promise();
  };

  Field.prototype.backup = function() {
    return null;
  };

  Field.prototype.refresh = function() {
  };
  
  return Field;
}); 
