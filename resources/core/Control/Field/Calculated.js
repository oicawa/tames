define(function (require) { 
  require("jquery");
  var Utils = require("core/Utils");
  var Locale = require("core/Locale");
  var Class = require("core/Class");
  var Storage = require("core/Storage");
  var Inherits = require("core/Inherits");
  var Field = require("core/Control/Field/Field");
  var Detail = require("core/Control/Detail");
  var DivButton = require("core/Control/DivButton");
  var Finder = require("core/Control/Finder");
  var Grid = require("core/Control/Grid");
  var Dialog = require("core/Dialog");
  var Uuid = require("core/Uuid");
  
  var TEMPLATE = '<div><div name="field" ></div></div>';

  function create_field(self, root) {
    var dfd = new $.Deferred;
    var field = root.find("div > div[name='field']");
    field.empty();
    var id = Uuid.version4();
    field.attr("id", id);
    
    if (is_null_or_undefined(self._fieldcontrol)) {
      dfd.resolve();
      return dfd.promise();
    }
    var Control = Field.control(self._fieldcontrol.require_path);
    self._control = new Control();
    var properties = self._properties.fieldcontrol.properties;
    self._control.init("#" + id, { name : "", label : "", datatype : { properties : properties }}) // Need xxx.datatype....
    .then(function () {
      self._control.edit(self.edit());
      dfd.resolve();
    });
    return dfd.promise();
  }
  
  function create_function(self, root) {
    var dfd = new $.Deferred;
    if (is_empty(self._properties.require_path) || is_empty(self._properties.function_name)) {
      dfd.reject();
      return dfd.promise();
    }
    
    require([self._properties.require_path], function(Module) {
      if (Module === null) {
        dfd.reject();
      }
      self._function = Module[self._properties.function_name];
      dfd.resolve();
    });
    return dfd.promise();
  }
  
  function Calculated() {
    Field.call(this, "core/Control/Field", "Calculated");
    this._selector = null;
    this._properties = null;
    this._fieldcontrol = null;
    this._function = null;
    this._value = null;
  }
  Inherits(Calculated, Field);

  Calculated.prototype.init = function(selector, field) {
    var dfd = new $.Deferred;

    // Assign to the members
    this._selector = selector;
    this._properties = field.datatype.properties;
    
    var root = $(selector);
    if (0 < root.children()) {
      dfd.resolve();
      return dfd.promise();
    }
    root.empty();
    root.append(TEMPLATE);

    // Create form tags
    var self = this;
    Storage.read(Field.FIELDCONTROL_ID, self._properties.fieldcontrol.id)
    .then(function(data) {
      if (is_null_or_undefined(data)) {
        throw { class_id : Field.FIELDCONTROL_ID, fieldcontrol_id : self._properties.fieldcontrol };
      }
      self._fieldcontrol = data;
    })
    .then(function() {
      return create_field(self, root);
    })
    .then(function () {
      return create_function(self);
    })
    .then(function() {
      return self._function();
    })
    .then(function(result) {
      self._value = result;
      dfd.resolve();
    })
    .catch(function (ex) {
      dfd.reject(ex);
    });
    return dfd.promise();
  };

  Calculated.prototype.edit = function(on) {
    // Do nothing. Calculated field is not editable.
  };
  
  Calculated.prototype.backup = function() {
    return this._value;
  };

  Calculated.prototype.commit = function() {
    if (this._control) {
      this._control.commit();
    }
    this._value = this.data();
  };

  Calculated.prototype.restore = function() {
    if (this._control) {
      this._control.restore();
    }
    // set value.properties into this._detail in event handler.
  };

  Calculated.prototype.data = function(value) {
    if (arguments.length == 0) {
      // Getter
      return is_null_or_undefined(this._control) ? null : this._control.data();
    } else {
      // Setter
      if (is_null_or_undefined(value) === false) {
        this._value = value;
      }
      this._control.data(this._value);
      this.refresh();
    }
  };
  
  Calculated.prototype.refresh = function() {
    if (this._control) {
      this._control.refresh();
    }
  };

  Calculated.renderer = function(field) {
    var dfd = new $.Deferred;
    var class_ = null;
    var fieldcontrols = null;
    $.when(
      Storage.read(Class.CLASS_ID, Field.FIELDCONTROL_ID).done(function(data) { class_ = data; }),
      Storage.read(Field.FIELDCONTROL_ID).done(function(data) { fieldcontrols = data; })
    )
    .then(function () {
      async function renderer(record, index, column_index) {
        var value = record[field.name];
        if (is_null_or_undefined(value)) {
          return "";
        }
        var field2 = { name : "data", datatype : { properties : field.datatype.properties } };
        var Control = Field.control(field.datatype.properties.fieldcontrol.id);
        var renderer2 = await Control.renderer(field2);
        return renderer2({ "data" : value});
      };
      dfd.resolve(renderer);
    })
    return dfd.promise();
  };

  return Calculated;
}); 
