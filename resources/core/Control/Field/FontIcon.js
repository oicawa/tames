define(function (require) { 
  require("jquery");
  var Utils = require("core/Utils");
  var Locale = require("core/Locale");
  var Storage = require("core/Storage");
  var Class = require("core/Class");
  var Inherits = require("core/Inherits");
  var Dialog = require("core/Dialog");
  var List = require("core/Control/List");
  var Field = require("core/Control/Field/Field");
  var Menu = require("core/Control/Menu");
  
  var WEBFONTS_SETS_ID = "43a28dff-bc30-452e-b748-08235443b7ce";
  var WEBFONTS_ONE_ID = "1af1bda1-2781-4991-9334-26bab3c0a58a"
  
  var TEMPLATE = '<div></div>';
  var FONT_TEMPLATE = '' +
'<span style="display:table;border:1px solid gray;border-radius:5px;margin:2px;height:60px;width:60px;">' +
'  <i style="display:table-cell;text-align:center;vertical-align:middle;padding:5px;" class="fa {{FONT_NAME}} fa-3x fa-fw"></i>' +
'</span>';

  function create_control(self, root, field) {
    root.empty();
    root.append(TEMPLATE);
    
    var div = root.find("div");
    var font = FONT_TEMPLATE.replace(/{{FONT_NAME}}/, "");
    div.append(font);
    
    self._icon = div.find("i");
    // Mouse over/out on icons.
    self._icon.on("mouseover", function(event) {
      var i = $(event.originalEvent.target);
      i.css("cursor", "pointer");
    });
    self._icon.on("mouseout", function(event) {
      var i = $(event.originalEvent.target);
      i.css("cursor", "auto");
    });
  }
  
  function FontIcon() {
    Field.call(this, "core/Control/Field", "FontIcon");
    this._fonts = null;
    this._icon = null;
    this._value = null;
    this._fixed = null;
  }
  Inherits(FontIcon, Field);

  FontIcon.prototype.init = function(selector, field) {
    var dfd = new $.Deferred;
    // Set member fields
    var root = $(selector);
    if (0 < root.children()) {
      dfd.resolve();
      return dfd.promise();
    }

    // Properties
    var prop = Utils.get_as_json(
      { font_icons_id : ["9c103467-759b-424a-9544-7bdd81030141"] },
      function () { return field.datatype.properties; }
    );
    var webfonts_id = is_array(prop.font_icons_id) ? prop.font_icons_id[0] : prop.font_icons_id;

    var fonts = null;
    var class_ = null;

    // Create control
    var self = this;
    $.when(
      Field.prototype.init.apply(self, arguments),
      Storage.read(WEBFONTS_SETS_ID, webfonts_id).done(function(data) { fonts = is_null_or_undefined(data) ? [] : data.fonts; }),
      Storage.read(Class.CLASS_ID, WEBFONTS_ONE_ID).done(function(data) { class_ = data; })
    ).then(function () {
      fonts.forEach(function (font, index) {
        font.recid = index + 1;
      });
      self._fonts = fonts;
      return;
    }).then(function () {
      return Class.field_map(class_).done(function (data) { self._field_map = data; });
    }).then(function () {
      create_control(self, root, field);
      dfd.resolve();
    });
    return dfd.promise();
  };
  
  FontIcon.prototype.edit = function(on) {
    if (!this._icon) {
      return;
    }
    if (!on) {
      this._icon.off("click");
      return;
    };
    
    var self = this;
    
    this._icon.on("click", function(event) {
      var dialog = new Dialog();
      //var grid = new Grid();
      var list = new List();
      var select_panel = "";
      dialog.init(function(panel_id) {
        return list.init("#" + panel_id, {
          "toolbar_items" : [],
          "width" : null,
          "height" : null,
          "class_id" : null,
          "embedded" : true,
          "columns" : [
            { field   : 'recid',
              caption : 'No.',
              size    : '40px'},
            { field   : 'id',
              caption : 'Icon',
              size    : '70px',
              render  : function(record, index, column_index) {
                var html = FONT_TEMPLATE.replace(/{{FONT_NAME}}/, record.id);
                return html;
              }},
            { field   : 'name',
              caption : 'Name',
              size    : '200px'}
          ],
          "field_map" : self._field_map,
          "comparers":{}
        });
      })
      .then(function() {
        list._grid.numbers(false);
        list._grid.row_height(70);
        list._grid.refresh();
      })
      .then(function() {
        var src_items = self._statics["toolbar"];
        if (is_null_or_undefined(src_items)) {
          return;
        }
        return Menu.convert(src_items, list).done(function(dst_items) { list.items(dst_items); });
      })
      .then(function() {
        list.data(self._fonts);
        list.refresh();
      })
      .then(function() {
        dialog.title(Locale.translate(self._statics["dialog_title"]));
        dialog.buttons([
          {
            text : "OK",
            click: function (event) {
              var recid = list._grid.selection();
              self._value = self._fonts[recid - 1].id;
              self.refresh();
              dialog.close();
            }
          },
          {
            text : "Cancel",
            click: function (event) {
              console.log("[Cancel] clicked.");
              dialog.close();
            }
          }
        ]);
        dialog.size(list.width() + 30, 600);
        dialog.open();
      });
    });
  };

  FontIcon.prototype.backup = function() {
    return this._fixed;
  };

  FontIcon.prototype.commit = function() {
    this._fixed = this._value;
  };

  FontIcon.prototype.restore = function() {
    this._value = this._fixed;
    this.refresh();
  };

  FontIcon.prototype.data = function(value) {
    if (arguments.length == 0) {
      return this._value;
    } else {
      this._value = value;
      this._fixed = value;
      this.refresh();
    }
  };
  
  FontIcon.prototype.refresh = function() {
    this._icon.attr("class", "");
    this._icon.addClass("fa");
    this._icon.addClass(this._value);
    this._icon.addClass("fa-3x");
    this._icon.addClass("fa-fw");
  }
  
  FontIcon.renderer = function(field) {
    var dfd = new $.Deferred;
    var renderer = function(record, index, column_index) {
      var value = record[field.name];
      return '<i style="display:inline;text-align:center;vertical-align:middle;padding:5px;" class="fa ' + value + ' fa-fw"></i>';
    };
    dfd.resolve(renderer);
    return dfd.promise();
  };
  FontIcon.container = function(field) {
    function contains(record, search_text) {
      var text = Locale.translate(record[field.name]);
      if (is_null_or_undefined(text)) {
        return false;
      }
      return (text.indexOf(search_text) <  0) ? false : true;
    }
    var dfd = new $.Deferred;
    dfd.resolve(contains);
    return dfd.promise();
  };

  return FontIcon;
}); 
