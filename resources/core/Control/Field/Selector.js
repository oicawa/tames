define(function (require) { 
  require("jquery");
  var Utils = require("core/Utils");
  var Locale = require("core/Locale");
  var Storage = require("core/Storage");
  var Inherits = require("core/Inherits");
  var Class = require("core/Class");
  var SelectDialog = require("core/Control/SelectDialog");
  var Grid = require("core/Control/Grid");
  var Finder = require("core/Control/Finder");
  var Field = require("core/Control/Field/Field");
  var Css = require("core/Css");
  
  var TEMPLATE = '<div></div>';

  function Selector() {
    Field.call(this, "core/Control/Field", "Selector");
    this._selector = null;
    this._properties = null;
    this._finder = null;
  }
  Inherits(Selector, Field);

  Selector.prototype.build = function() {
    if (is_empty(this._properties.class_id)) {
      return this._finder.init(this._selector + " > div", [], {}, [], "", false, 200, null);
    }
    var class_ = null;
    var field_map = null;
    var columns = null;
    var items = null;
    var self = this;
    var dfd = new $.Deferred;
    $.when(
      Storage.read(Class.CLASS_ID, this._properties.class_id).then(function(data) { class_ = data; }),
      Storage.read(this._properties.class_id).then(function(objects) { items = objects; })
    )
    .then(function () {
      return Class.field_map(class_).then(function (field_map_) { field_map = field_map_; });
    })
    .then(function () {
      columns = Grid.columns(class_, field_map);
    })
    .then(function() {
      return (new Class(class_)).renderer();
    })
    .then(function(renderer) {
      return self._finder.init(self._selector + " > div", columns, field_map, items, self._properties.description, self._properties.multi_selectable, self._properties.min_width, renderer);
    })
    .then(function() {
      dfd.resolve();
    });
    return dfd.promise();
  };

  Selector.prototype.init = function(selector, field) {
    var dfd = new $.Deferred;
    
    this._selector = selector;
    
    var root = $(selector);
    if (0 < root.children()) {
      dfd.resolve();
      return dfd.promise();
    }
    
    // Create form tags
    root.empty();
    root.append(TEMPLATE);
    // Finder
    this._finder = new Finder();
    
    this._properties = is_null_or_undefined(field.datatype.properties) ? { class_id : "", description : "", multi_selectable : false, min_width : 200 } : field.datatype.properties;
    
    var self = this;
    Css.load("core/Control/Field/Selector.css")
    .then(function () {
      return self.build();
    })
    .then(function() {
      self.edit(false);
      dfd.resolve();
    });
    return dfd.promise();
  };
  
  Selector.prototype.edit = function(on) {
    this._finder.edit(on);
  };

  Selector.prototype.backup = function() {
    return this._finder.backup();
  };

  Selector.prototype.commit = function() {
    this._finder.commit();
  };

  Selector.prototype.restore = function() {
    this._finder.restore();
  };

  Selector.prototype.data = function(value) {
    if (arguments.length == 0) {
      return this._finder.data();
    }
    this._finder.data(value);
    this.refresh();
  };
  
  Selector.prototype.refresh = function() {
    this._finder.refresh();
  };

  Selector.renderer = function(field) {
    var dfd = new $.Deferred;
    Finder.renderer(field)
    .done(function (renderer) {
      dfd.resolve(renderer);
    });
    return dfd.promise();
  };

  return Selector;
}); 
