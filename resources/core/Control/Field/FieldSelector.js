define(function (require) {
  require("jquery");
  var app = require("core/app");
  var Utils = require("core/Utils");
  var Locale = require("core/Locale");
  var Class = require("core/Class");
  var Storage = require("core/Storage");
  var Inherits = require("core/Inherits");
  var Field = require("core/Control/Field/Field");
  var Grid = require("core/Control/Grid");
  var Finder = require("core/Control/Finder");
  
  var TEMPLATE = '' +
'<div>' +
'  <div name="class"></div>' +
'  <div name="field"></div>' +
'</div>';

  function FieldSelector() {
    Field.call(this, "core/Control/Field", "FieldSelector");
    this._selector = null;
    this._value = null;
    this._default_class_id = null;
    this._classes = null;
    this._class = { _class:null, columns:null, finder:null, converter:null };
    this._field = { _class:null, columns:null, finder:null, converter:null };
  }
  Inherits(FieldSelector, Field);

  FieldSelector.prototype.init = function(selector, field) {
    var dfd = new $.Deferred;
    // Selector & Root
    this._selector = selector;
    var root = $(selector);
    if (0 < root.children()) {
      dfd.resolve();
      return dfd.promise();
    }
    root.empty();
    root.append(TEMPLATE);

    // Finders
    this._class.finder = new Finder();
    this._field.finder = new Finder();
    
    var properties = field.datatype.properties;
    var min_width = !properties ? null : properties.min_width;
    this._default_class_id = !properties ? null : properties.default_class_id;

    var self = this;
    
    Storage.read(Class.CLASS_ID)
    .then(function(classes) {
      self._classes = classes;
      self._class._class = self._classes[Class.CLASS_ID];
      self._field._class = self._classes[Class.FIELD_ID];
    })
    .then(function () {
      return Class.field_map(self._class._class).then(function (field_map) { self._class.field_map = field_map; });
    })
    .then(function () {
      return Class.field_map(self._field._class).then(function (field_map) { self._field.field_map = field_map; });
    })
    .then(function () {
      self._class.columns = Grid.columns(self._class._class, self._class.field_map);
    })
    .then(function () {
      self._field.columns = Grid.columns(self._field._class, self._field.field_map);
    })
    .then(function () {
      return (new Class(self._class._class)).renderer();
    })
    .then(function (renderer) {
      return self._class.finder.init(selector + " > div > div[name='class']", self._class.columns, self._class.field_map, self._classes, "Class", false, min_width, renderer);
    })
    .then(function () {
      return (new Class(self._field._class)).renderer();
    })
    .then(function (renderer) {
      return self._field.finder.init(selector + " > div > div[name='field']", self._field.columns, self._field.field_map, {}, "Field", false, min_width, renderer);
    })
    .then(function () {
      function load_field_parameters(class_id) {
        var fields = {};
        self._classes[class_id].object_fields.forEach(function (object_field, index) {
          var field = Utils.clone(object_field);
          field.recid = field.name;
          field.id = index;
          fields[field.name] = field;
        });
        self._field.finder._objects = fields;
        self._field.finder.clear();
      }
      if (is_empty(self._default_class_id)) {
        self._class.finder.ok(function (recids) {
          var recid = recids[0];
          load_field_parameters(recid);
        });
      } else {
        load_field_parameters(self._default_class_id);
      }
    })
    .then(function () {
      dfd.resolve();
    });
    return dfd.promise();
  };

  FieldSelector.prototype.edit = function(on) {
    if (arguments.length == 0) {
      return this._field.finder._editting;
    }

    if (is_empty(this._default_class_id))
      this._class.finder.edit(on);
    this._field.finder.edit(on);
  };
  
  FieldSelector.prototype.backup = function() {
    var class_id = this._class.finder.backup();
    var field_name = this._field.finder.backup();
    return { "class_id" : class_id, "field_name" : field_name };
  };

  FieldSelector.prototype.commit = function() {
    this._class.finder.commit();
    this._field.finder.commit();
  };

  FieldSelector.prototype.restore = function() {
    this._class.finder.restore();
    this._field.finder.restore();
  };

  FieldSelector.prototype.data = function(value) {
    if (arguments.length == 0) {
      var class_id = this._class.finder.data();
      var field_name = this._field.finder.data();
      return { "class_id" : class_id, "field_name" : field_name };
    }

    var self_class_id = null;
    if (is_empty(this._default_class_id) === false) {
      self_class_id = this._default_class_id;
    }
    
    if (is_null_or_undefined(value) && is_empty(this._default_class_id)) {
      this._field.finder._objects = {};
      this.refresh();
      return;
    }

    var class_id = is_empty(self_class_id) ? value.class_id : self_class_id;
    var field_name = is_null_or_undefined(value) ? null : value.field_name;
    this._class.finder.data(class_id);
    this._field.finder.data(field_name);
    var objects = {};
    var class_ = this._classes[class_id];
    if (is_null_or_undefined(class_) === false) {
      class_.object_fields.forEach(function (field) {
        if (field.recid)
          delete field.recid;
        field.id = field.name;
        objects[field.id] = field;
      });
    }
    this._field.finder._objects = objects;
    this.refresh();
  };
  
  FieldSelector.prototype.refresh = function() {
    this._class.finder.refresh();
    this._field.finder.refresh();
  };

  FieldSelector.renderer = function(field) {
    var dfd = new $.Deferred;
    var classes = null;
    $.when(
      Storage.read(Class.CLASS_ID).done(function(data) { classes = data; })
    ).always(function() {
      var renderer = function(record, index, column_index) {
        var value = record[field.name];
        var class_ = classes[value.class_id];
        if (!class_) {
          return "/";
        }
        var fields = class_.object_fields.filter(function (field_) { return field_.name == value.field_name });
        var field_ = fields[0];
        return Locale.translate(class_.label) + "/" + (!field_ ? "" : Locale.translate(field_.label));
      };
      dfd.resolve(renderer);
    });
    return dfd.promise();
  };

  return FieldSelector;
});
