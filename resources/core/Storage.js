define(function (require) {
  require("jquery");
  var Utils = require('core/Utils');
  var Connector = require('core/Connector');
  var Locale = require('core/Locale');

  var base_url = 'api/rest/';
  var CLASS_ID = null;
  var session = window.sessionStorage;
  session.clear();
  
  function get_all(class_id) {
    var objects_string = session.getItem(class_id);
    if (!objects_string) {
      return null;
    }
    var objects = JSON.parse(objects_string);
    if (!objects) {
      return null;
    }
    return objects;
  }
  
  function get_one(class_id, object_id) {
    var objects = get_all(class_id);
    if (!objects) {
      return null;
    }
    var object = objects[object_id];
    return !object ? null : object;
  }
  
  function set_all(class_id, objects) {
    console.assert(is_object(objects), objects);
    if (!is_object(objects)) {
      console.error("'objects' argument is not [Object].");
      console.dir(objects);
      return;
    }
    session.setItem(class_id, JSON.stringify(objects));
  }
  
  function set_one(class_id, object_id, object) {
    console.assert(is_object(object), object);
    if (!is_object(object)) {
      console.error("'object' argument is not [Object].");
      console.dir(object);
      return;
    }
    var objects = get_all(class_id);
    if (!objects) {
      objects = {};
    }
    objects[object_id] = object;
    set_all(class_id, objects);
  }
  
  function remove_all(class_id) {
    session.removeItem(class_id);
  }
  
  function remove_one(class_id, object_id) {
    var objects = get_all(class_id);
    if (!objects) {
      return;
    }
    delete objects[object_id];
    set_all(class_id, objects);
  }

  function is_cached(class_) {
    var props = class_.options.properties;
    if (props === null) {
      console.debug("1. props === null");
      return false;
    }
    if (is_null_or_undefined(props.cache)) {
      console.debug("2. props.cache = " + props.cache);
      return false;
    }
    return props.cache;
  }

  function set_all_session_objects() {
    var tmp = Object.values(get_all(CLASS_ID));
    var classes = Object.values(get_all(CLASS_ID)).filter(is_cached);
    console.debug("classes count = " + classes.length);
    var promises = classes.map(function(class_) {
      var url = base_url + class_.id
      var inner_dfd = new $.Deferred;
      Connector.get(url, "json")
      .done(function (response, text_status, jqXHR) {
        console.debug("[OK] " + Locale.translate(class_.label));
        set_all(class_.id, response);
        inner_dfd.resolve();
      })
      .fail(function (jqXHR, text_status, error_thrown) {
        console.error("[NG] " + Locale.translate(class_.label));
        inner_dfd.reject(jqXHR, text_status, error_thrown);
      });
      return inner_dfd.promise();
    });
    var dfd = new $.Deferred;
    $.when.apply(null, promises)
    .then(function() {
      dfd.resolve();
    });
    return dfd.promise();
  }

  var Storage = {
    init : function(class_id) {
      CLASS_ID = class_id;
      var url = base_url + CLASS_ID;
      var dfd = new $.Deferred;
      Connector.get(url, "json")
      .done(function (response, text_status, jqXHR) {
        var class_ = response[CLASS_ID];
        console.debug("[OK] " + Locale.translate(class_.label));
        return set_all(CLASS_ID, response);
      }).fail(function (jqXHR, text_status, error_thrown) {
        console.error("[NG] Starting Class objects");
        dfd.reject(jqXHR, text_status, error_thrown);
      }).then(function () {
        return set_all_session_objects();
      }).then(function () {
        dfd.resolve();
      });
      return dfd.promise();
    },
    create: function(class_id, object, files) {
      var dfd = new $.Deferred;
      
      var url = base_url + class_id;
      Connector.post(url, object, files, "json")
      .done(function (response, text_status, jqXHR) {
        var class_ = get_one(CLASS_ID, class_id);
        if (is_cached(class_)) {
          set_one(class_id, response.id, response);
        }
        dfd.resolve(response, text_status, jqXHR);
      })
      .fail(function (jqXHR, text_status, error_thrown) {
        dfd.reject(jqXHR, text_status, error_thrown);
      });
      return dfd.promise();
    },
    read: function(class_id, object_id) {
      var dfd = new $.Deferred;
      
      var class_only = is_null_or_undefined(object_id);
      var class_ = get_one(CLASS_ID, class_id);
      var cached = is_cached(class_);

      // If cached data, read from session objects.
      if (cached) {
        var data = class_only ? get_all(class_id) : get_one(class_id, object_id);
        dfd.resolve(data);
        console.debug("[OK] (cache)" + Locale.translate(class_.label) + (class_only ? "" : ("/" + object_id)));
        return dfd.promise();
      }

      // If not cached data, read from server.
      var url = base_url + class_id + (class_only ? '' : '/' + object_id);
      Connector.get(url, "json")
      .done(function (response, text_status, jqXHR) {
        console.debug("[OK] (server)" + Locale.translate(class_.label) + (class_only ? "" : ("/" + object_id)));
        dfd.resolve(response);
      })
      .fail(function (jqXHR, text_status, error_thrown) {
        console.debug("[NG] (server)" + Locale.translate(class_.label) + (class_only ? "" : ("/" + object_id)));
        dfd.reject(jqXHR, text_status, error_thrown);
      });
      return dfd.promise();
    },
    update: function(class_id, object_id, object, files) {
      var url = base_url + class_id + '/' + object_id;
      var dfd = new $.Deferred;
      Connector.update(url, object, files, "json")
      .done(function (response, text_status, jqXHR) {
        var class_ = get_one(CLASS_ID, class_id);
        if (is_cached(class_)) {
          set_one(class_id, response.id, response);
        }
        dfd.resolve(response, text_status, jqXHR);
      })
      .fail(function (jqXHR, text_status, error_thrown) {
        if (jqXHR.status == 410) {
          remove_one(class_id, object_id);
        }
        dfd.reject(jqXHR, text_status, error_thrown);
      });
      return dfd.promise();
    },
    delete: function(class_id, object_id) {
      var url = base_url + class_id + '/' + object_id;
      var dfd = new $.Deferred;
      Connector.delete(url, "json")
      .done(function (response, text_status, jqXHR) {
        remove_one(class_id, object_id);
        dfd.resolve(response, text_status, jqXHR);
      })
      .fail(function (jqXHR, text_status, error_thrown) {
        if (jqXHR.status == 410) {
          remove_one(class_id, object_id);
        }
        dfd.reject(jqXHR, text_status, error_thrown);
      });
      return dfd.promise();
    },
    personal : function (key, data) {
      var dfd = new $.Deferred;
      if (arguments.length === 1) {
        var target = window.localStorage[key]
        var data = is_null_or_undefined(target) ? target : JSON.parse(target);
        dfd.resolve(data);
        return dfd.promise();
      }
      window.localStorage[key] = JSON.stringify(data);
      dfd.resolve();
      return dfd.promise();
    }
  };
  
  return Storage;
});
